#include "mia.h"

/* mialign: 2-way aligner using mia's alignment engine.
   by Adam Novak
*/

/* Takes a pointer to an Alignment
   that has valid sequence, length, submat, and sg data
   Does dynamic programming, filling in values in the 
   a->m dynamic programming matrix
   Returns nothing
   This variant computes an ends-free alignment: it's not 
   allowed to start the alignment at arbitrary points in 
   the interior of the matrix. Combined with only checking 
   the last row/column for alignment endpoints, this makes the alignments ends-free.
 */
void dyn_prog_endsfree( AlignmentP a ) {
  int row, 
    col, 
    gap_col_score, 
    gap_row_score,
    diag_score,
    hp_disc_gap_col_score,
    hp_disc_gap_row_score,
    sm_depth;
  size_t i;
  int HIM = (INT_MIN / 2); // half of the minimum int; this
  //is useful to avoid underflow from subtracting from the
  // smallest possible int
  int row_sm[5]; // row substitution matrix
  
  /* Initialize */
  row = 0;
  col = 0;
  hp_disc_gap_col_score = HIM;
  hp_disc_gap_row_score = HIM;

  /* Set up the substitution matrix scores for this base for
     this first row */
  for( i = 0; i <= 4; i++ ) {
    row_sm[i] = a->submat->sm[0][i][a->s2c[0]];
  }

  // First row, no penalty whether sg or not
  for( col = 0; col < a->len1; col++ ) {
    if ( a->align_mask[col] ) {
      a->m->mat[row][col].score =
        row_sm[a->s1c[col]];

      //      a->m->mat[row][col].score = 
      //sub_mat_score(a->s1c[col], a->s2c[row],
      //              a->submat->sm, row, a->len2);
    }
    else {
      a->m->mat[row][col].score = HIM;
    }
    a->m->mat[row][col].trace = 0; // any alignment
    //traced back this far must start here, just stick
    //in a 0 for the heck of it
    a->best_gap_row[col] = 0;
  }

  // Subsequent rows, 
  for ( row = 1; row < a->len2; row++ ) {
    
    /* Set up the substitution matrix scores for the base 
       for this row */
    sm_depth = find_sm_depth( row, a->len2 );
    for( i = 0; i <= 4; i++ ) {
      row_sm[i] = a->submat->sm[sm_depth][i][a->s2c[row]];
    }
    /* First column, special case - no gapping
       in either direction and pay a penalty for
       unaligned bases if sg */
    col = 0;
    /*
      assert( a && a->m && a->m->mat && a->s1c && a->s2c && a->submat ) ;
      assert( 0 <= row && row < a->len2 ) ;
      assert( 0 <= col && col < a->len1 ) ;
    */
    if ( a->align_mask[col] ) {
      /* a->m->mat[row][col].score = 
        sub_mat_score(a->s1c[col], a->s2c[row],
        a->submat->sm, row, a->len2); */
      a->m->mat[row][col].score =
        row_sm[a->s1c[col]];
      // pay penalty at col 0 if sg
      if ( a->sg5 ) {
        a->m->mat[row][col].score 
          -= (GOP + (GEP * (row+1)));
      }
    }

    else {
      a->m->mat[row][col].score = HIM;
    }

    a->m->mat[row][col].trace = 0;

    // Subsequent columns
    a->best_gap_col = 0;
    for( col = 1; col < a->len1; col++ ) {
      if ( a->align_mask[col] ) {
        /*        a->m->mat[row][col].score = 
          sub_mat_score(a->s1c[col], a->s2c[row],
          a->submat->sm, row, a->len2); */
        a->m->mat[row][col].score =
          row_sm[a->s1c[col]];
        
        /* update best_gap_col by comparing new gap
           option to previous best, if we're far 
           enough in to gap columns
        */
        if ( col >= 2 ) {
          if ( (a->m->mat[row-1][col-2].score - (GOP + GEP)) >
               (a->m->mat[row-1][a->best_gap_col].score - 
                (GOP + (GEP * (col-(a->best_gap_col)-1)))) ) {
            a->best_gap_col = col - 2;
          }
          gap_col_score = 
            ( a->m->mat[row-1][a->best_gap_col].score -
              (GOP + (GEP * (col - a->best_gap_col - 1))) );
        }
        else {
          gap_col_score = HIM;
        }

        /* update best_gap_row by comparing new gap
           option to previous best, if we're far enough
           down to gap rows
        */
        if ( row >= 2 ) {
          if ( (a->m->mat[row-2][col-1].score - (GOP + GEP)) > 
               (a->m->mat[a->best_gap_row[col-1]][col-1].score -
                (GOP + (GEP * (row-(a->best_gap_row[col-1])-1)))) ) {
            a->best_gap_row[col-1] = row - 2;
          }
          gap_row_score = 
            ( a->m->mat[a->best_gap_row[col-1]][col-1].score -
              (GOP + (GEP * (row-(a->best_gap_row[col-1])-1))) );
        }
        else {
          gap_row_score = HIM;
        }
        
        /* Find diagonal score */
        diag_score = a->m->mat[row-1][col-1].score;
        
        /* Calculate special homopolymer discount? */
        if ( a->hp ) {
          hp_disc_gap_col_score = HIM;
          hp_disc_gap_row_score = HIM;
          if ( a->seq1[col] == a->seq2[row] ) { // must be the same base
            if ( (a->hprs[row] == row) && // seq1 hp starts here
                 (a->hpcs[col] != col) && // seq2 hp starts before here
                 (a->hpcs[col] > 0) // can't gap outside of seq1!
                 ) {
              hp_disc_gap_col_score = 
                (a->m->mat[row-1][(a->hpcs[col]-1)].score -
                 hp_discount_penalty( (col - a->hpcs[col]),
                                      a->hpcl[col], a->hprl[row] ));
            }
            if ( (a->hpcs[col] == col) && // seq2 hp starts here
                 (a->hprs[row] != row) && // seq1 hp starts before here
                 (a->hprs[row] > 0) ) { // can't gap outside of seq2!
              hp_disc_gap_row_score = 
                (a->m->mat[(a->hprs[row]-1)][col-1].score -
                 hp_discount_penalty( (col - a->hpcs[col]),
                                      a->hpcl[col], a->hprl[row] ));
            }
          }
        }
        
        /* Now best options are on the table, pick the
           best of the best */

        /* Best option is continuing on the diagonal */
        if ( (diag_score >= gap_col_score) &&
             (diag_score >= gap_row_score) &&
             (diag_score >= hp_disc_gap_col_score) &&
             (diag_score >= hp_disc_gap_row_score)
             ) {
          a->m->mat[row][col].trace = 0;
          a->m->mat[row][col].score += diag_score;
        }
        
        else {
          /* Is best option gapping back columns? */
          if ( (gap_col_score >= gap_row_score) &&
               (gap_col_score >= hp_disc_gap_col_score) &&
               (gap_col_score >= hp_disc_gap_row_score)
               ) {
            a->m->mat[row][col].score += gap_col_score;
            a->m->mat[row][col].trace = a->best_gap_col;
          }
          
          else {
            if ( (gap_row_score >= hp_disc_gap_col_score) &&
                 (gap_row_score >= hp_disc_gap_row_score) ) {
              /* Best option must be gapping up rows */
              a->m->mat[row][col].score += gap_row_score;
              a->m->mat[row][col].trace = 
                -(a->best_gap_row[col-1]);
            }
            else {
              if ( hp_disc_gap_col_score >= hp_disc_gap_row_score ) {
                /* Best option is homopolymer discounted gapping
                   of columns */
                a->m->mat[row][col].score += hp_disc_gap_col_score;
                a->m->mat[row][col].trace = a->hpcs[col] - 1;
              }
              else {
                /* Best option is homopolymer discounted gapping
                   of rows */
                a->m->mat[row][col].score += hp_disc_gap_row_score;
                a->m->mat[row][col].trace = -(a->hprs[row] - 1);
              }
            }
          }
        }
      }
      else {
        a->m->mat[row][col].score = HIM;
        a->m->mat[row][col].trace = 0;
      }
    }

    /* Now, end of row, so pay penalty for unaligned
       seq1, if sg3 alignment */
    if ( (a->sg3) &&
         (a->len1 > (row+1)) ) {
      a->m->mat[row][col].score -= 
        GOP + ((a->len1 - row - 1) * GEP);
    }
  }
}

/* Return the maximum of two integers.
 */
int imax(int a, int b) {
    return a > b ? a : b;
}

/* Input is a pointer to a valid Alignment. The values
   in a->len1 and a->len2 must be valid.
   Searches the last row (a->len1 -1) and column (a->len2 - 1) 
   along all columns and rows to find the best score.
   Sets a->aec and a->aer to the correct values and sets
   a->best_score to the best score.
   Returns the best score */
int max_score ( AlignmentP a ) {
  int row, col;
  int best_score = INT_MIN;
  
  row = a->len2 - 1;
  if ( row < 0 ) {
    /* Yeah, it happens if the sequence length is 0 */
    return best_score;
  }

  /* When finding the max_score, keep the earlier best score in
     case of a tie.
     This ensures that an alignment that is
     only against end-wrapped sequence will not be used if there
     is the same alignment earlier */
  for ( col = 0; col < a->len1; col++ ) {
    if ( a->m->mat[row][col].score > best_score ) {
      a->aec = col;
      a->aer = row;
      best_score = a->m->mat[row][col].score;
    }
  }
  
  /* Now try the last column to see if we get a better score. */
  col = a->len1 - 1;
  if ( col < 0 ) {
    /* Yeah, it happens if the sequence length is 0 */
    return best_score;
  }
  
  for ( row = 0; row < a->len2; row++ ) {
    if ( a->m->mat[row][col].score > best_score ) {
      a->aec = col;
      a->aer = row;
      best_score = a->m->mat[row][col].score;
    }
  }
  
  a->best_score = best_score;
  return best_score;
}

/* Go through a->m->mat[row][col].trace and compute the length of the alignment.
   Length is the length of the string bounded by between the first and last 
   aligned bases.
   Assumes a->aer, a->aec, a->abr, and a->abc are populated.
 */
size_t alignment_length(AlignmentP a) {
  // Calculate the length of the alignment
  int length = 0;
  
  int row, col;
  
  row = a->aer;
  col = a->aec;
  while( (a->m->mat[row][col].trace != col) &&
         (a->m->mat[row][col].trace != -row) ) {
    if ( a->m->mat[row][col].trace == 0 ) { // Match/mismatch
      length++;
      row--;
      col--;
      
    }
    else {
      if ( a->m->mat[row][col].trace < 0 ) { // Gap in ref
        length += (row - -(a->m->mat[row][col].trace));
        row = -(a->m->mat[row][col].trace);
        col--;
        
      }
      else { // Gap in frag
        length += (col - a->m->mat[row][col].trace);
        col = a->m->mat[row][col].trace;
        row--;
      }
    }
  }
  
  length += 1; // Count the last character.
  
  return length;
}

/* Go through a->m->mat[row][col].trace and compute the gapped strings for the 
   reference and the fragment. Then output them in forward order as a FASTA 
   alignment.
   Calls fasta_aln_print.
*/
void print_aligned_fasta(AlignmentP a, char* ref_id, char* frag_id) {
  
  int row, col;
  
  // Get the alignment's length
  int length = alignment_length(a);
  
  // How long are the unaligned bits?
  int ref_unaligned_start = a->abc;
  int ref_unaligned_end = a->len1 - a->aec - 1;
  int frag_unaligned_start = a->abr;
  int frag_unaligned_end = a->len2 - a->aer - 1;
  
  // Only one sequence should have unaligned bases at either end.
  if(ref_unaligned_start && frag_unaligned_start) {
    fprintf( stderr, "Error: Reference and fragment both overhang start.\n" );
    exit( 0 );
  }
  if(ref_unaligned_end && frag_unaligned_end) {
    fprintf( stderr, "Error: Reference and fragment both overhang end.\n" );
    exit( 0 );
  }
  
  // How many extra alignment columns at each end do we need to hold this?
  int unaligned_start = imax(ref_unaligned_start, frag_unaligned_start);
  int unaligned_end = imax(ref_unaligned_end, frag_unaligned_end);
  
  fprintf( stderr, "Overhangs: %d at start and %d at end\n", unaligned_start, 
    unaligned_end );
    
  // So how many alignment columns total?
  int alignment_columns = unaligned_start + length + unaligned_end;
  
  // Now reserve space for the alignment
  char* aligned_ref = (char*) save_malloc( (alignment_columns + 1) 
    * sizeof(char) );
  if ( aligned_ref == NULL ) {
    fprintf( stderr, "Could not allocate aligned reference sequence.\n" );
    exit(0);
  }
  // Anywhere we don't write letters is a gap
  memset( aligned_ref, '-', alignment_columns ); 
  aligned_ref[alignment_columns] = '\0';
  
  char* aligned_frag = (char*) save_malloc( (alignment_columns + 1) 
    * sizeof(char) );
  if ( aligned_frag == NULL ) {
    fprintf( stderr, "Could not allocate aligned fragment sequence.\n" );
    exit(0);
  }
  // Anywhere we don't write letters is a gap
  memset( aligned_frag, '-', alignment_columns ); 
  aligned_frag[alignment_columns] = '\0';
  
  // Fill in the alignment strings
  // Start just after the end of the alignment, and do any trailing unaligned 
  // stuff
  row = a->aer + 1;
  col = a->aec + 1;
  
  // This index starts at the end of the alignment strings, and advances left
  int aligned_index = alignment_columns;
  
  // First, put whichever sequence overhangs on this end, if any.
  aligned_index -= unaligned_end;
  if( ref_unaligned_end ) {
    // Some reference sequence overhangs the end
    strncpy(&aligned_ref[aligned_index], &a->seq1[col], unaligned_end);
  } else if( frag_unaligned_end ) {
    // Some fragment sequence overhangs the end
    strncpy(&aligned_frag[aligned_index], &a->seq2[row], unaligned_end);
  }
  
  // Move back to aer, aec, and move aligned_index to where the last aligned 
  // base goes
  row--;
  col--;
  aligned_index--;
  
  // Then, put in the very last aligned character.
  aligned_ref[aligned_index] = a->seq1[col];
  aligned_frag[aligned_index] = a->seq2[row];
  
  // Then put in the rest of the alignment.
  while( (a->m->mat[row][col].trace != col) &&
         (a->m->mat[row][col].trace != -row) ) {    
    // Compute where in the strings we should put the next characters
    // Trace is "previous aligned position that gave the best score"
    // So we need to say the bases *at the coordiantes specified by trace* 
    // are aligned. *not* the bases here.
    
    // Go to the pair of bases specified by trace     
    if ( a->m->mat[row][col].trace == 0 ) { // Match/mismatch
      row--;
      col--;
    }
    else {
      if ( a->m->mat[row][col].trace < 0 ) { // Gap in ref
        // Put the skipped bases in frag
        int newRow = -(a->m->mat[row][col].trace);
        // Skip 1 fewer chars because this points to one before the beginning of
        // what we need to copy.
        int skipped = row - newRow - 1; 
        
        // Where does the chunk need to start in the destination?
        aligned_index -= skipped;
        // Where does it come from? (newRow + 1) in a->seq2
        strncpy( &aligned_frag[aligned_index], &a->seq2[newRow + 1], 
          skipped );
        
        row = newRow;
        col--;
      }
      else { // Gap in frag
        // Put the skipped bases in ref
        int newCol = a->m->mat[row][col].trace;
        // Skip 1 fewer chars because this points to one before the beginning of
        // what we need to copy.
        int skipped = col - newCol - 1;
        
        // Where does the chunk need to start in the destination?
        aligned_index -= skipped;
        // Where does it come from? (newCol + 1) in a->seq1
        strncpy( &aligned_ref[aligned_index], &a->seq1[newCol + 1], skipped );
        
        col = newCol;
        row--;
      }
    }
    
    // Compute where next aligned character pair lives.
    aligned_index--;
    
    // Put in the characters to say they are aligned.
    aligned_ref[aligned_index] = a->seq1[col];
    aligned_frag[aligned_index] = a->seq2[row];
  }

  // Put any unaligned sequence that overhangs the beginning of the 
  // alignment.
  aligned_index -= unaligned_start;
  if( ref_unaligned_start ) {
    // Some reference sequence overhangs the start
    col -= unaligned_start;
    strncpy( &aligned_ref[aligned_index], &a->seq1[col], unaligned_start );
  } else if( frag_unaligned_start ) {
    // Some fragment sequence overhangs the start
    row -= unaligned_start;
    strncpy( &aligned_frag[aligned_index], &a->seq2[row], unaligned_start );
  }

  // Now row and col both should be 0, as should aligned_index.
  if( row != 0 || col != 0 || aligned_index != 0 ) {
    fprintf( stderr, "Error: tracing sequence did not complete cleanly.\n" );
    fprintf( stderr, "Row: %d, Col: %d, aligned_index: %d\n", row, col, 
      aligned_index );
    exit( 0 );
  }

  // Print the sequences
  fasta_aln_print( aligned_ref, ref_id );
  fasta_aln_print( aligned_frag, frag_id );
  
  // Free memory
  free( aligned_ref );
  free( aligned_frag );
}

void help( void ) {
  printf( "\n\n%s -- MIAlign %s\n",PACKAGE_NAME, PACKAGE_VERSION);
  printf( "       A pairwise ends-free aligner.\n\n");
  printf( "Copyright Richard E. Green, Michael Siebauer, Adam Novak 2008-2012\n");
  printf( "Report bugs to <%s>.\n",PACKAGE_BUGREPORT);
  printf( "===============================+++++++++++++==\n");
  printf( "\nUsage:\n");
  printf( "mialign -r <reference sequence FASTA>\n" );
  printf( "        -f <query sequence FASTA or FASTQ>\n" );
  printf( "        -s <substitution matrix file> (if not supplied an default matrix is used)\n" );
  printf( "The default substitution matrix used the following parameters:\n" );
  printf( "  MATCH=%d, MISMATCH=%d, N=%d for all positions\n", FLAT_MATCH, FLAT_MISMATCH, N_SCORE);
}

int main( int argc, char* argv[] ) {

  char ref_fn[MAX_FN_LEN+1]; // Filename of reference sequence
  char frag_fn[MAX_FN_LEN+1]; // Filename of query sequence
  char mat_fn[MAX_FN_LEN+1]; // Filename of substitution matrix
  char* test_id;

  
  // Alignment we are doing
  AlignmentP align; 
  
  // Substitution matrix
  PSSMP ancsubmat   = init_flatsubmat();
  
  FragSeqP frag_seq;

  // Argparse stuff
  extern int optind;
  extern char* optarg;
  
  // Loop variable
  int i;


  /* Process command line arguments */
  int ich; // Current option
  int any_arg = 0; // Were there any options passed?
  while( (ich=getopt( argc, argv, "r:f:s:" )) != -1 ) {
    switch(ich) {
    case 'r' :
      strcpy( ref_fn, optarg );
      any_arg = 1;
      break;
    case 'f' :
      strcpy( frag_fn, optarg );
      any_arg = 1;
      break;
    case 's' :
      strcpy( mat_fn, optarg );
      free( ancsubmat ); // trash the flat submat we initialized with
      ancsubmat   = read_pssm( mat_fn );
      any_arg = 1;
      break;
    default :
      help();
      exit( 0 );
    }
  }

  if ( !any_arg ) {
    help();
    exit( 0 );
  }

  if ( optind != argc ) {
    fprintf( stderr, "There seems to be some extra cruff on the command line that mialign does not understand.\n" );
    exit(0);
  }

  /* Start the clock... */
  time_t curr_time = time(NULL);

  /* Announce that we're starting */
  fprintf( stderr, 
           "Starting alignment of %s\n and %s\n at %s\n", 
           ref_fn, frag_fn,
           asctime(localtime(&curr_time)) );

  /* Initialize the reference sequence, as init_map_alignment() does */
  // Allocate memory for the RefSeq
  RefSeqP refseq = (RefSeqP) save_malloc(sizeof (RefSeq));
  if (refseq == NULL) {
      fprintf( stderr, "Could not allocate reference sequence.\n" );
      exit(0);
  }
  // Zero-out the RefSeq
  for (i = 0; i <= MAX_ID_LEN; i++) {
      refseq->id[i] = '\0';
  }
  for (i = 0; i <= MAX_DESC_LEN; i++) {
      refseq->desc[i] = '\0';
  }
  refseq->seq = NULL;
  refseq->rcseq = NULL;
  refseq->size = 0;
  refseq->gaps = NULL;
  refseq->circular = 0;
  refseq->wrap_seq_len = 0;

  /* Read in the reference sequence, but skip reverse complement */
  if ( read_fasta_ref( refseq, ref_fn ) != 1 ) {
    fprintf( stderr, "Problem reading reference sequence file %s\n", ref_fn );
    exit( 1 );
  } else {
    fprintf( stderr, "Reference sequence read successfully.\n");
  }

  /* Assume it is not circular */
  refseq->wrap_seq_len = refseq->seq_len;

  /* Add space for the gaps array */
  fprintf( stderr, "Setting up gaps array...\n");
  refseq->gaps = (int*)save_malloc((refseq->wrap_seq_len+1) *
                                      sizeof(int));
  for( i = 0; i <= refseq->wrap_seq_len; i++ ) {
    refseq->gaps[i] = 0;
  }

  /* We're skipping kmer arrays. Just upper case the reference sequences. */
  make_ref_upper( refseq );

  /* Set up FragSeqP to point to a FragSeq */
  frag_seq = (FragSeqP)save_malloc(sizeof(FragSeq));

  /* Set up the alignment structure */
  fprintf( stderr, "Initializing alignment object (%d by %d)...\n", 
    INIT_ALN_SEQ_LEN, (refseq->wrap_seq_len + (2*INIT_ALN_SEQ_LEN)) );
  align = (AlignmentP)init_alignment( INIT_ALN_SEQ_LEN,
                                         (refseq->wrap_seq_len + 
                                          (2*INIT_ALN_SEQ_LEN)),
                                         0, 0 ); // Not reverse-complement, no homopolymer discount
  
  if( align == NULL) {
    fprintf( stderr, "Could not allocate memory for alignment.\n" );
      exit(0);
  }

  /* Configure the alignment semantics */
  fprintf( stderr, "Configuring alignment...\n");  
  align->submat = ancsubmat;
  align->sg5 = 0;
  align->sg3 = 0;
  align->seq1 = refseq->seq;
  align->len1 = refseq->seq_len;
  
  

  /* Put the s1c lookup codes in the alignment. Not reallys sure what they do, 
     but they're probably important. */
  fprintf( stderr, "Populating s1c lookup codes...\n");
  pop_s1c_in_a( align ); // "pop" stands for "populate"

  /* One by one, go through the input file of fragments to be aligned.
     Align them to the reference. For each fragment generating an
     alignment score better than the cutoff, merge it into the maln
     alignment. Keep track of those that don't, too. */
  FILE* FF;
  int seq_code = 0; // code to indicate sequence input format; 0 => fasta; 1 => fastq
  
  fprintf( stderr, "Reading fragment sequence...\n");
  FF = fileOpen( frag_fn, "r" );
  seq_code = find_input_type( FF );

  /* Only read the first sequence from the query sequence file. */
  if(!read_next_seq( FF, frag_seq, seq_code )) {
    fprintf( stderr, "Problem reading query sequence file %s\n", ref_fn );
    exit( 1 );
  }
    
  if ( DEBUG ) {
    fprintf( stderr, "%s\n", frag_seq->id );
  }
  
  fclose( FF );

  /* Announce we're strarting alignment */
  fprintf( stderr, "Starting to align sequences...\n" );

  /* Attach query sequence to alignment */
  align->seq2 = frag_seq->seq;
  align->len2 = frag_seq->seq_len;
  pop_s2c_in_a( align );

  /* Run the alignment */
  dyn_prog_endsfree( align );
  int best_score = max_score( align );
  fprintf( stderr, "Best score: %d\n", best_score );
  find_align_begin( align );
  int length = alignment_length( align );
  
  
  fprintf( stderr, "Alignment found from %d, %d to %d, %d (length %d)\n",
           align->abr, align->abc, align->aer, align->aec, length );
  fprintf( stderr, "(These are the first and last aligned bases.)\n" );
  /* Spit out the aligned sequences */
  print_aligned_fasta( align, refseq->id, frag_seq->id);

  /* Announce we're finished */
  curr_time = time(NULL);
  fprintf( stderr, "Alignment finished at %s\n",
           asctime(localtime(&curr_time)) );

  // No leaking memory
  free(refseq->gaps);
  free(refseq);
  free(frag_seq);
  free(ancsubmat);

  exit( 0 );
}
